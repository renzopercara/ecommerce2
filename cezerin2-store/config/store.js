const fs = require('fs');
const dotenv = require('dotenv');
if (process.env.NODE_ENV !== undefined){
	console.log('.env.' + process.env.NODE_ENV);
	const envConfig = dotenv.parse(fs.readFileSync('.env.' + process.env.NODE_ENV.trim()));
	for (const k in envConfig) {
		process.env[k] = envConfig[k];
	}
}
else
	dotenv.config();

console.log('#########API_BASE_URL' + process.env.API_BASE_URL);
console.log('#######STORE_PORT: ' + process.env.STORE_PORT);

// config used by store client side only
module.exports = {
	// store UI language
	language: process.env.LANGUAGE || 'en',
	// used by Store (server side)
	ajaxBaseUrl: process.env.AJAX_BASE_URL || 'http://localhost:3001/ajax'
};
